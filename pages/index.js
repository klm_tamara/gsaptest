import { SmoothScrollProvider } from "../src/components/scroll";
import React, { useRef } from "react";
import gsap from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import GlobalStyles from "../src/style/global";

gsap.registerPlugin(ScrollTrigger);

const Box = () => (
  <div
    className="square"
    style={{ width: "500px", height: "500px", background: "white" }}
  />
);

export default function IndexPage() {
  const squareRef = useRef();
  const boxCount = 7;

  return (
    <>
      <GlobalStyles />
      <SmoothScrollProvider boxCount={boxCount}>
        <div
          style={{
            height: "100vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            background: "salmon",
          }}
        >
          <div className="text">
            <h1>Section 1</h1>
          </div>
        </div>

        <div
          style={{
            position: "relative",
            minHeight: `${100 + (boxCount > 3 ? boxCount - 3 : 0) * 30}vh`,
            background: "palegreen",
            boxSizing: "border-box",
            borderRadius: "0px 50px 0px 0px",
          }}
          className="gsap-container"
        >
          <div
            style={{
              minHeight: `${100 + (boxCount > 3 ? boxCount - 3 : 0) * 30}vh`,
              position: "absolute",
              inset: "0",
              left: "30%",
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "flex-start",
              boxSizing: "border-box",
              paddingTop: "300px",
              gap: "20px",
            }}
            className="square-container"
          >
            {Array(boxCount)
              .fill()
              .map((_, i) => (
                <Box key={i} />
              ))}
          </div>
        </div>
        <div
          style={{
            height: "100vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            background: "#ddddff",
          }}
        ></div>
      </SmoothScrollProvider>
    </>
  );
}
